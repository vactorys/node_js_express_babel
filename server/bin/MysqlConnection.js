import * as dotenv from 'dotenv';
import mysql from 'mysql2'

class MysqlConnection {

    /**
     * 
     * @param {String} db_name 
     */
    constructor(db_name = 'default') {
        this.env = dotenv.config()
        this.connection = mysql.createConnection(
            { 
                host: this.env.parsed[`db.${db_name}.host`], 
                database: this.env.parsed[`db.${db_name}.database`], 
                user: this.env.parsed[`db.${db_name}.user`], 
                password: this.env.parsed[`db.${db_name}.password`], 
                port: this.env.parsed[`db.${db_name}.port`]
            }
        );

        this.stmt = '';

        this.bindVal = [];
    }

    /**
     * 
     * @param {String} fields 
     * @param {String} table_name 
     * @returns 
     */
    select(fields, table_name){
        this.stmt += `SELECT `;

        if (table_name == '' || table_name == undefined) {
            throw new Error('table_name cannot be empty');
        }

        if (fields == '' || fields == undefined){
            this.stmt += `* `;
        }else{
            this.stmt += `${fields} `;
        }

        this.stmt += `from ${table_name} `;

        return this;
    }

    /**
     * 
     * @param {String} table_name 
     * @param {Object} value_cond
     * @returns 
     */
    update(table_name, value_cond){
        this.stmt += `UPDATE ${table_name} SET`;

        for (const key in value_cond) {
            this.stmt += ` ${key} = ? ,`;
            this.bindVal.push(value_cond[key]);
        }

        this.stmt = this.stmt.slice(0, this.stmt.length - 1);
    
        return this;
    }

    /**
     * 
     * @param {String} table_name 
     * @returns 
     */
    delete(table_name){
        this.stmt += `DELETE FROM ${table_name} `;
        return this;
    }

    /**
     * 
     * @param {String} column_name 
     * @param {String} expression 
     * @param {String} value 
     * @returns 
     */
    where(column_name, expression, value){
        if (!this.stmt.includes('where')) {
            this.stmt += ` where `;
        }
        
        this.stmt += `${column_name} ${expression} ? `;

        this.bindVal.push(value);

        return this
    }

    /**
     * 
     * @param {String} column_name 
     * @param {String} expression 
     * @param {String} value 
     * @returns 
     */
    and(column_name, expression, value){
        
        this.stmt += `AND ${column_name} ${expression} ? `;

        if (!this.stmt.includes('where')) {
            console.error('please use function where First!!');
            throw new Error(`SQL ERROR Not Have Where ${this.stmt}`)
        }

        this.bindVal.push(value);

        return this
    }

    /**
     * 
     * @param {String} column_name 
     * @param {String} expression 
     * @param {String} value 
     * @returns 
     */
    or(column_name, expression, value){
        
        this.stmt += `OR ${column_name} ${expression} ? `;

        if (!this.stmt.includes('where')) {
            console.error('please use function where First!!');
            throw new Error(`SQL ERROR Not Have Where ${this.stmt}`)
        }

        this.bindVal.push(value);
        
        return this
    }

    /**
     * 
     * @param {String} table_name 
     * @param {String} on_statement 
     * @param {String} type_join 
     * @returns 
     */
    join(table_name, on_statement, type_join){
        this.stmt += `${type_join} JOIN ${table_name} ON ${on_statement} `;

        return this
    }

    /**
     * 
     * @param {String} query 
     * @returns 
     */
    setQuery(query){
        this.stmt = query;

        return this;
    }

    /**
     * 
     * @returns Promise
     */
    result(){
        return new Promise((resolve, reject) => {
            this.connection.execute(this.stmt, this.bindVal, function (err, result, fields) {
                if (err) reject(err);
                let data = result[0];
                resolve(data);
            });
        }); 
    }

    /**
     * 
     * @returns Promise
     */
    resultAll(){
        return new Promise((resolve, reject) => {
            this.connection.execute(this.stmt, this.bindVal, function (err, result, fields) {
                if (err) reject(err);
                let data = result;
                resolve(data);
            });
        }); 
    }

}

export default MysqlConnection;